# GEGEN Self installation

Processing video installation created by Pixelflowers for the pool area at GEGEN Self, KitKat Club, Berlin, 3.1.2020

Font used: [Anonymous Pro](https://fonts.google.com/specimen/Anonymous+Pro)

The random sentence generator was also used as a source in a VJ set and in the [video](https://archive.org/details/GEGEN-SELF)  projected in the main floor of GEGEN Self.