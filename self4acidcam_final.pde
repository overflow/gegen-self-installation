import processing.video.*;

int displayWidth = 1280;
int displayHeight = 800; 
Capture cam;
int y;
int x;
PFont font;
float textmaxwidth;
float textmaxheight;
float textsize;
int time = millis();
int videoScale = 15;
int cols, rows;

color rosso = color(255,50,0);
color bianco = color(255,255,255);
color blu = color(100,0,255);
color ocra = color(215,200,50);
color verde = color(100,200,100);
color fucsia = color(255,150,200);
color arancione = color(255,150,0);
color grigio = color(100);
color azzurro = color(100,150,255);
color viola = color(150,0,255);

color giallo = color(249,250,28);
color pink = color(310,69,97);
color arancio = color(252,178,87);
color lilla = color(233,142,247);

color[] colarray = {            //random selects one of above colors
  rosso, bianco, blu, ocra, verde, fucsia, arancione, grigio, azzurro, viola
};
color[] colarraybg = {            //random selects one of above colors
  verde, fucsia, arancio, azzurro, giallo, pink, lilla
};
color col;
color colbg;



void setup() {
  fullScreen(1);
  //size(displayWidth, displayHeight);
  cols = width / videoScale;
  rows = height / videoScale;
  colorMode(RGB, 255, 255, 255, 100);
  cam = new Capture(this, 800, 600, Capture.list()[1]);
  cam.start();
  smooth();
  background(255, 255, 255);
  frameRate(5);
  cam.loadPixels();
  loadPixels();
  font = loadFont("AnonymousPro-Bold-48.vlw");
  sentence();
}

void sentence() {
  String[] Verbs = { 
  "   be", "annihilate", "embrace", " trust", "distrust", "repudiate", "celebrate", "  shun", "  love", "ignore", "despise", "nurture", "dismiss",
  "  hate", "commiserate", "give up on", "laugh at", "challenge", "resist", "explore", "reclaim", "forget", "ignore", "dream", "hallucinate"
};
  String[] Nouns = { 
  "old self", "        self", "real self", "other self", "self-righteousness", "selfishness", "self-control", "self-consciousness", "self-image", "self-respect", "self-restraint",
  "self-abandonment", "self-denial", "self-sacrifice", "self-abuse", "self-esteem", "self-glory", "self-promotion", "self-alienation", "self-awareness", "self-care",
  "self-censorship", "selflessness", "self-devotion", "self-critique", "self-deception", "self-defense", "self-delusion", "self-destruction", "self-display", "self-forgetfulness",
  "self-fulfillment", "self-gratification", "self-help", "self-hypnosis", "self-identity", "self-importance", "self-indulgence", "self-loathing", "self-medication", "self-management",
  "self-perception", "self-pity", "self-preservation", "self-punishment", "self-reflection", "self-regard", "self-reliance", "self-reproach", "self-sacrifice", "self-scrutiny",
  "self-stimulation", "self-surrender", "self-torment", "self-transformation", "self-validation", "self-worth"
  };

  int verb = int(random(Verbs.length));
  int noun = int(random(Nouns.length));

  String Sentence = Verbs[verb]+"\n    your \n"+Nouns[noun];
  float sentlength = textWidth(Sentence);
  
  textAlign(LEFT);
  textFont(font);
  //smooth();
  textsize = random(width/30, width/15);
  textmaxwidth = random(10, sentlength-15);
  textmaxheight = random(0+textsize, height-(textsize*2));

  textSize(textsize);
  strokeWeight(10);
  color col = colarray[int(random(colarray.length))];
      fill(255,255,255);
      text(Sentence, textmaxwidth+0.5, textmaxheight-0.5);
      fill(col);
      text(Sentence, textmaxwidth, textmaxheight);
}



void draw() {
  
  
  if (cam.available()) {
    cam.read();
    
  }
  

    color colbg = colarraybg[int(random(colarraybg.length))];  //assigns a random color from above to c1-4
 
  filter(INVERT);
  tint(colbg,30);
  image(cam, 0, 0, width, height);
  

  cam.loadPixels();
  for (int i = 0; i < cols; i++) {    
    // Begin loop for rows    
    for (int j = 0; j < rows; j++) {      
      // Where are you, pixel-wise?      
      int x = i*videoScale;      
      int y = j*videoScale;    
      
      // Reverse the column to mirro the image.
      int loc = (cam.width - i - 1) + j * cam.width;       
      
      color c = cam.pixels[loc];
      // A rectangle's size is calculated as a function of the pixel’s brightness. 
      // A bright pixel is a large rectangle, and a dark pixel is a small one.
      float sz = (brightness(c)/255.0) * videoScale;       
          
      fill(0, 50);     
      noStroke();      
      ellipse(x + videoScale/2, y + videoScale/2, sz, sz);
    }

  }
  
  
   blendMode(REPLACE);
  if (millis() > time + 5000)
  {
    sentence();
    time = millis();
  }
  

  
 // the following line is to save frames
 // saveFrame("frames/frame-######.jpg");
  
}