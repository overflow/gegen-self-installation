//import spout.*;

// DECLARE A SPOUT OBJECT
//Spout spout;

int displayWidth = 800;
int displayHeight = 600;
PFont font;
float textmaxwidth;
float textmaxheight;
float textsize;
float xpos;
float ypos;

color rosso = color(255,50,0);
color blu = color(100,0,255);
color ocra = color(215,200,50);
color verde = color(100,200,100);
color fucsia = color(255,150,200);
color arancione = color(255,150,0);
color grigio = color(100);
color azzurro = color(100,150,255);
color viola = color(150,0,255);

color[] colarray = {            //random selects one of above colors
  rosso, blu, ocra, verde, fucsia, arancione, grigio, azzurro, viola
};
color col;

void setup() {
  size(displayWidth, displayHeight, P2D);
  textureMode(NORMAL);
  smooth();
  background(255,255,255, 5);
  
  //spout = new Spout(this);
  //spout.createSender("Spout frasi");
  
  frameRate(0.2);
  font = loadFont("AnonymousPro-Bold-48.vlw");
  color col = colarray[int(random(colarray.length))];
  sentence();
}

void sentence() {
  String[] Verbs = { 
  "   be", "annihilate", "embrace", " trust", "distrust", "repudiate", "celebrate", "  shun", "  love", "ignore", "despise", "nurture", "dismiss",
  "  hate", "commiserate", "give up on", "laugh at", "challenge", "resist", "explore", "reclaim", "forget", "ignore", "dream", "hallucinate"
};
  String[] Nouns = { 
  "old self", "        self", "real self", "other self", "self-righteousness", "selfishness", "self-control", "self-consciousness", "self-image", "self-respect", "self-restraint",
  "self-abandonment", "self-denial", "self-sacrifice", "self-abuse", "self-esteem", "self-glory", "self-promotion", "self-alienation", "self-awareness", "self-care",
  "self-censorship", "selflessness", "self-devotion", "self-critique", "self-deception", "self-defense", "self-delusion", "self-destruction", "self-display", "self-forgetfulness",
  "self-fulfillment", "self-gratification", "self-help", "self-hypnosis", "self-identity", "self-importance", "self-indulgence", "self-loathing", "self-medication", "self-management",
  "self-perception", "self-pity", "self-preservation", "self-punishment", "self-reflection", "self-regard", "self-reliance", "self-reproach", "self-sacrifice", "self-scrutiny",
  "self-stimulation", "self-surrender", "self-torment", "self-transformation", "self-validation", "self-worth"
  };

  int verb = int(random(Verbs.length));
  int noun = int(random(Nouns.length));

  String Sentence = Verbs[verb]+"\n    your \n"+Nouns[noun];
  println(Sentence);
  float sentlength = textWidth(Sentence);
  textAlign(LEFT);
  textFont(font);

  textsize = random(displayWidth/20, displayWidth/15);
  textmaxwidth = random(10, sentlength-15);
  textmaxheight = random(0+textsize, height-(textsize*2));
  textSize(textsize);


  strokeWeight(10);
  color col = colarray[int(random(colarray.length))];

  for (int b = 0; b <  10; b++)
    {
      fill(col);
      text(Sentence, textmaxwidth, textmaxheight);
  }
}



void draw() {
  
  //spout.sendTexture();
  

  fill(255,255,255,20);
  rect(0,0,displayWidth,displayHeight);
  
  noStroke();      
  blendMode(REPLACE);
  if (frameCount % 2 == 0)
  {
    sentence();
  }
    
  
 // the following line is to save frames
  //saveFrame("framescol/frame-######.jpg");
  
}